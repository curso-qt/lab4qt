#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <QFileSystemModel>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    auto *model = new QFileSystemModel{this};
    model->setRootPath(QDir::homePath());
    ui->treeView->setModel(model);
    ui->treeView->setRootIndex(model->index(QDir::homePath()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

